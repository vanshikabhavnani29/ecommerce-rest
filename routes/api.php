<?php
use App\Http\Controllers\SellerTransactionController;
use App\Http\Controllers\SellerCategoryController;
use App\Http\Controllers\SellerBuyerController;
use App\Http\Controllers\SellerProductController;
use App\Http\Controllers\CategoryBuyerController;
use App\Http\Controllers\CategoryProductController;
use App\Http\Controllers\CategorySellerController;
use App\Http\Controllers\CategoryTransactionController;
use App\Http\Controllers\BuyerProductsController;
use App\Http\Controllers\BuyerCategoryController;
use App\Http\Controllers\BuyersController;
use App\Http\Controllers\BuyerSellerController;
use App\Http\Controllers\BuyerTransactionsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProductTransactionController;
use App\Http\Controllers\ProductBuyerController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductBuyerTransactionController;
use App\Http\Controllers\SellersController;
use App\Http\Controllers\TransactionCategoryController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\TransactionSellerController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::resource('buyers', BuyersController::class)->only('index', 'show');
// Route::resource('buyers', BuyersController::class, ['only' => ['index', 'show']);
Route::resource('sellers', SellersController::class)->only('index', 'show');
Route::resource('users', UsersController::class)->only('index', 'store', 'update', 'show', 'destroy');
Route::get('users/verify/{token}', [UsersController::class, 'verify'])->name('users.verify');
Route::get('users/{user}/resend-verification-email', [UsersController::class, 'resend'])->name('users.resend');
Route::resource('categories', CategoriesController::class)->except('create', 'edit');
Route::resource('products', ProductsController::class)->only('index', 'show');
Route::resource('products.transactions', ProductTransactionController::class)->only('index');
Route::resource('products.buyers', ProductBuyerController::class)->only('index');
Route::resource('products.categories', ProductCategoryController::class)->only('index', 'update', 'destroy');
Route::resource('products.buyers.transactions', ProductBuyerTransactionController::class)->only('store');
Route::resource('transactions', TransactionsController::class)->only('index', 'show');

Route::resource('categories.products', CategoryProductController::class)->only('index');
Route::resource('categories.sellers', CategorySellerController::class)->only('index');
Route::resource('categories.transactions', CategoryTransactionController::class)->only('index');
Route::resource('categories.buyers', CategoryBuyerController::class)->only('index');

Route::resource('sellers.transactions', SellerTransactionController::class)->only('index');
Route::resource('sellers.categories', SellerCategoryController::class)->only('index');
Route::resource('sellers.buyers', SellerBuyerController::class)->only('index');
Route::resource('sellers.products', SellerProductController::class)->except('eidt','create','show');

Route::resource('transactions.categories', TransactionCategoryController::class)->only('index');
Route::resource('transactions.sellers', TransactionSellerController::class)->only('index');
Route::resource('buyers.transactions', BuyerTransactionsController::class)->only('index');
Route::resource('buyers.products', BuyerProductsController::class)->only('index');
Route::resource('buyers.sellers', BuyerSellerController::class)->only('index');
Route::resource('buyers.categories', BuyerCategoryController::class)->only('index');

Laravel\Passport\Passport::routes();
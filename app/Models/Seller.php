<?php

namespace App\Models;
use App\Transformers\SellerTransformer;
use App\Scopes\SellerScopes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Seller extends User
{
    use HasFactory;
    protected $table = 'users';
    public string $transformer = SellerTransformer::class;
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SellerScopes());
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}

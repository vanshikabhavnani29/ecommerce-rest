<?php

namespace App\Models;
use App\Transformers\BuyerTransformer;
use App\Scopes\BuyerScopes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Buyer extends User
{
    use HasFactory;
    protected $table = 'users';
    public string $transformer = BuyerTransformer::class;
    public function __construct()
    {
        array_push($this->hidden, 'admin');
    }
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new BuyerScopes());
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}

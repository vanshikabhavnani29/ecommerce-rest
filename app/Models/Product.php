<?php

namespace App\Models;
use App\Transformers\ProductTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use HasFactory, SoftDeletes;
	const AVAILABLE_PRODUCT = 1;
	const UNAVAILABLE_PRODUCT = 2;
	public string $transformer = ProductTransformer::class;
	protected $fillable = [
		'name',
		'description',
		'quantity',
		'status',
		'image',
		'seller_id'
		//'price' will change from time to time hence we need to track time with price
		//2 options
		//store in transactions for each product
		//create another table prices (price, stDate, endDate, product)
	];

    protected $hidden = [
        'pivot'
    ];

    public static function boot(){
    	parent::boot();
    	self::updated(function(Product $product){
    		if($product->quantity == 0 && $product->isAvailable()){
    			$product->status = self::UNAVAILABLE_PRODUCT;
    			$product->save();
    		}
    	});
    }

	public function isAvailable()
	{
		return $this->status == Product::AVAILABLE_PRODUCT;
		//making status integer
		//integer comparision O(1)
		//string comparision O(n)
	}

	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}

	public function seller()
	{
		return $this->belongsTo(Seller::class);
	}

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}

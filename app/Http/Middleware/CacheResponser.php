<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CacheResponser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $url = request()->url();
        $queryParameters = request()->query();
        $method = request()->getMethod();
        ksort($queryParameters);
        $queryString = http_build_query($queryParameters);
        $fillUrl = "$method:{$url}?{$queryString}";
        if(Cache::has($fillUrl)){
            return new Response(Cache::get($fillUrl));
        }
        return $next($request);
    }
}

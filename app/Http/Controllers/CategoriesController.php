<?php
//saare categories ka details
namespace App\Http\Controllers;

use App\Models\Category;
use App\Traits\ResponseHelper;
use Illuminate\Http\Request;
use App\Transformers\CategoryTransformer;

class CategoriesController extends ApiController
{
    use ResponseHelper;
    public function __construct()
    {
        $this->middleware('transform.input:' . CategoryTransformer::class)->only('store', 'update'); 
        $this->middleware('client.credentials')->only('index','show');  //bearer token agar 3rd party ke paas hoga toh hi they can view the categories
        $this->middleware('auth:api')->except('index','show');
    }
    public function index()
    {
        $categories = Category::all();
        return $this->showAll($categories);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2',
            'description' => 'required|min:2'
        ];

        $this->validate($request, $rules);

        $category = Category::create($request->only('name', 'description'));
        return $this->showOne($category, 201);
    }

    public function show(Category $category)
    {
        return $this->showOne($category);
    }

    public function update(Request $request, Category $category)
    {
        $category->fill($request->only(['name', 'description']));

        if($category->isClean()) {
            return $this->errorResponse("You need to change something to update", 422);
        }

        $category->save();
        return $this->showOne($category);
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return $this->showOne($category);
    }
}

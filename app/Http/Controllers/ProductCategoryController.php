<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Transformers\CategoryTransformer;

class ProductCategoryController extends ApiController
{
	public function __construct()
    {
        $this->middleware('transform.input:' . CategoryTransformer::class)->only('update'); 
        $this->middleware('client.credentials')->only('index');  
        $this->middleware('auth:api')->except('index');
        $this->middleware('can:add-category,product')->only('update');
        $this->middleware('can:delete-category,product')->only('destroy');
    }
	public function index(Product $product){
		$categories = $product->categories;
		return $this->showAll($categories);
	}

	public function update(Request $request, Product $product, Category $category){
		$product->categories()->syncWithoutDetaching($category->id);
		$categories = $product->categories;
		return $this->showAll($categories);
	}

	public function destroy(Product $product, Category $category){
		if(! $product->categories()->find($category->id)){
			return $this->errorResponse('the prodict doesnot belong to the specified category', 404);
		}

		$product->categories()->detach($category->id);
		$categories = $product->categories;
		return $this->showAll($categories);
	}
}
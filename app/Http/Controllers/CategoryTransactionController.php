<?php
//ye category ke product ka transaction || category ke product ko kisne kharida
namespace App\Http\Controllers;

use App\Models\Category;

class CategoryTransactionController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
    }
    public function index(Category $category) {
        $transactions = $category   ->products()
                                    ->with('transactions')
                                    ->get()
                                    ->pluck('transactions')
                                    ->flatten();
        return $this->showAll($transactions);
    }
}

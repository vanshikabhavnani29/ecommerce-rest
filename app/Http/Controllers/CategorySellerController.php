<?php
//ye category kaunsa seller provide kar rha hai
namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategorySellerController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
    }
    public function index(Category $category) {
        $sellers = $category->products()
                            ->with('seller')
                            ->get()
                            ->pluck('seller')
                            ->unique()
                            ->values();
        return $this->showAll($sellers);
    }
}

<?php
//buyer ne jo prod kharida wo kaunse seller ka hai
namespace App\Http\Controllers;

use App\Models\Buyer;

class BuyerSellerController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
    }
    public function index(Buyer $buyer)
    {
        $sellers = $buyer->transactions()
                        ->with('product.seller')
                        ->get()
                        ->pluck('product.seller')
                        ->unique();

        return $this->showAll($sellers);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Transaction;

class TransactionsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth:api')->only('index','show');
        $this->middleware('scope:read-general')->only('show');
        $this->middleware('can:view,transaction')->only('show');
    }
    public function index()
    {
        $transactions = Transaction::all();
        return $this->showAll($transactions);
    }
    public function show(Transaction $transaction)
    {
        return $this->showOne($transaction);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Product;

class ProductsController extends ApiController
{
    public function __construct(){
        $this->middleware('client.credentials')->only('index', 'show');
    }
    public function index()
    {
        $products = Product::all();
        return $this->showAll($products);
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return $this->showOne($product);
    }
}

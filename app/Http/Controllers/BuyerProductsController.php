<?php
//buyer ne kaunse products kharide hai
namespace App\Http\Controllers;

use App\Models\Buyer;

class BuyerProductsController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('can:view,buyer')->only('index');
    }
    public function index(Buyer $buyer)
    {
        $products = $buyer->transactions()
                            ->with('product')
                            ->get() //collection of transaction with product
                            ->pluck('product');

        return $this->showAll($products);
    }
}

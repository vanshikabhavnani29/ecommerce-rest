<?php
//buyer ne jo bhi product kharida wo traansaction
namespace App\Http\Controllers;

use App\Models\Buyer;

class BuyerTransactionsController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('can:view,buyer')->only('index');
    }
    public function index(Buyer $buyer)
    {
        $transactions = $buyer->transactions;
        return $this->showAll($transactions);
    }
}

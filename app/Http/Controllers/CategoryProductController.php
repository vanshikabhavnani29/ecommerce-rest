<?php
//ye category ke kaunse products hai
namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;


class CategoryProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
    	$this->middleware('client.credentials')->only('index');
    }
    public function index(Category $category) {
        $products = $category->products()->get();
        return $this->showAll($products);
    }
}

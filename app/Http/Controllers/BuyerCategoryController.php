<?php
//buyer ne kaunse kaunse category ke kharide hain
namespace App\Http\Controllers;

use App\Models\Buyer;

class BuyerCategoryController extends ApiController
{
    public function __construct(){
        $this->middleware('auth:api')->only('index'); //login ke baad hi dikhna chahiye
        $this->middleware('scope:read-general')->only('index'); // user ke token me permission hai to read general files 
        $this->middleware('can:view,buyer')->only('index'); 
        //can calls BuyerPolicy ka view function with buyer as argument that means agar wo user is a buyer toh hi he/she can view 
    }
    public function index(Buyer $buyer)
    {
        $categories = $buyer->transactions()
                            ->with('product.categories')
                            ->get()
                            ->pluck('product.categories')
                            ->collapse()
                            ->unique('id')
                            ->values();

        return $this->showAll($categories);
    }
}

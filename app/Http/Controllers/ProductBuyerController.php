<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductBuyerController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
    }
	public function index(Product $product){
		$buyers = $product->transactions()
					->with('buyer')
					->get()
					->pluck('buyer')
					->unique('id');
		return $this->showAll($buyers);
	}
}
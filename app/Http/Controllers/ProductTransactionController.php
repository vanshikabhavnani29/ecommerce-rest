<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductTransactionController extends ApiController
{
	public function index(Product $product){
		$transactions = $product->transactions;
		return $this->showAll($transactions);
	}
}
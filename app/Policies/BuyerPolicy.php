<?php

namespace App\Policies;

use App\Models\Buyer;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyerPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability){
        if($user->isAdmin()){
            return true;
        }
    }
    
    public function view(User $user, Buyer $buyer)
    {
        return $user->id === $buyer->id; //user is authenticated and buyer is argument se aayega
    }
    
    public function purchase(User $user, Buyer $buyer)
    {
        return $user->id === $buyer->id;
    }

}

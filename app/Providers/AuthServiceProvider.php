<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use \Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        App\Models\Buyer::class => App\Policies\BuyerPolicy::class,
        App\Models\Seller::class => App\Policies\SellerPolicy::class,
        App\Models\User::class => App\Policies\UserPolicy::class,
        App\Models\Transaction::class => App\Policies\TransactionPolicy::class,
        App\Models\Product::class => App\Policies\ProductPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensExpireIn(now()->addMinutes(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));

        Passport::tokensCan([
            'purchase-product' => 'Create a new transaction for a specific product',
            'manage-account' => 'read and modify account data but not delete',
            'manage-product' => 'CRUD for products',
            'read-general' => 'read general files like categories, purchased products, your transaction'
        ]);
    }
}

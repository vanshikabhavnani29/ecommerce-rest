@component('mail::message')
# Hello, {{$user->name}}

Your email has been updated kindly verify using the below link

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

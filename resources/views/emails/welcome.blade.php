@component('mail::message')
# Hello, {{$user->name}}

Thankyou for creating an account with us

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
